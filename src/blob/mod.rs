// blob/mod.rs
//! Provides `Blob`, the atom of `note`.
use juniper;
use juniper::{FieldResult, FieldError, Value};


//use juniper::{FieldResult, FieldError, Value};
//use std::hash::{Hasher, Hash};

use std::collections::hash_map::{HashMap};
use std::cell::RefCell;
use std::fmt::Debug;

use super::hash_default;

//#[derive(GraphQLEnum, Debug, Clone, Hash)]
pub enum Val<T,U> {
    Blob(T),
    Span(U),
}

pub type BlobId = String;

#[derive(Hash, Clone, Debug, PartialEq, Eq)]
//#[graphql(description="a humanoid creature")] // enable when 
pub struct Blob {
    id: BlobId,
    content: String,
    points_to: Vec<BlobId>,
    is_pointed_to_by: Vec<BlobId>
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct NewBlob {
    pub content: String,
    pub points_to: Vec<BlobId>,
    pub is_pointed_to_by: Vec<BlobId>,
}

impl NewBlob {
    pub fn into_blob(self, id: &str) -> Blob {
        Blob {
            id: id.to_owned(),
            content: self.content,
            points_to: self.points_to,
            is_pointed_to_by: self.is_pointed_to_by,
        }
    }

    pub fn into_blob_t((x, id): (NewBlob, &str)) -> Blob {
        x.into_blob(id)
        
    }

    pub fn push_to(&mut self, id: &str) {
        self.points_to.push(id.to_owned());
    }

    pub fn push_to_by(&mut self, id: &str) {
        self.is_pointed_to_by.push(id.to_owned());
    }
}


// TODO:
// □  error handling. now. _christ_ man. 
graphql_object!(Blob: Storage |&self| {
    description: "A Blob in the database."

    // NB: use `fn ... -> T as "<description>" to queryable type descriptions (in
    // place of doc comments).
    field id() -> &str as "unique user id" { &self.id } 
    field content() -> &str as "user name" {&self.content }
    // use this field when references should be followed. This returns the
    // referent. if references are the desired output see `points_to_ids`.
    field points_to(&executor) -> Vec<Blob> { 
        let ctx = executor.context().vmap.borrow();
        let mut ret = vec![];
        for bid in self.points_to.iter() {
            if let Some(ref x) = ctx.get(bid) {
                //todo: convert to Blob
                let blob = (*x).clone().into_blob(bid);
                ret.push(blob);
            }
        }
        show(&ret);
        ret
    }
    // use this accessor/field-thunk when a list references (read: `BlobId`s)
    // should _not_ be followed.
    field points_to_ids(&executor) -> Vec<BlobId> { 
        // WHY is `self.points_to` empty?
        let ctx = executor.context().vmap.borrow();
        let mut ret = vec![];
        show(&self.points_to);
        for bid in self.points_to.iter() {
            if ctx.contains_key(bid) {
                ret.push(bid.to_owned());
            }
        }
        show(&ret);
        ret
    }
});


/// `db :: Key -> Val` map where `Val = Blob Int String | Span Selection BlobId`.
#[derive(Clone, Debug)]
pub struct Storage {
    pub vmap: RefCell<HashMap<BlobId, NewBlob>>,
//    pub names: RefCell<HashMap<String, (NewHuman, String)>>,
}

impl juniper::Context for Storage {}

impl Storage {
    pub fn new() -> Self {
        let vmap = RefCell::new(HashMap::new());
        Storage { vmap }
    }

    // Wrapper around `HashMap::insert`. Returns a tuple containing the
    // generated `BlobId` (a.t.m. hashed via `DefaultHasher`), and optionally,
    // the replaced `NewBlob`.
    pub fn insert(&mut self, v: NewBlob) -> (BlobId, Option<NewBlob>){
        let id = hash_default(&v).to_string();
        match self.vmap.borrow_mut().insert(id.clone(), v) {
            None => (id, None),
            Some(x) => (id, Some(x)),
        }
    }
    

    pub fn get_by_name(&self, _name: &str) -> Option<Blob> {
        unimplemented!();
    }
    
    /// Fetches `NewBlob` associated with the given `id: BlobId` and returns an
    /// optional `Blob` reconstructed with `id`.
    pub fn get(&self, id: &str) -> Option<Blob> {
        self.vmap.borrow().get(id).map(|new_blob| {
            (*new_blob).clone().into_blob(id)
        })
    }

    /// links two ids such that: id0 -> (read: points to) id1.
    pub fn link(&mut self, id0: &str, id1: &str) {
        // update subj
        self.vmap.borrow_mut()
                 .get_mut(id0)
                 .map(|new_blob| {
                     // b0 is pointed to by b1
                     new_blob.push_to_by(id1);
                 });
        // update obj
        self.vmap.borrow_mut()
                 .get_mut(id1)
                 .map(|new_blob| {
                     // b1 points to b0
                     new_blob.push_to(id0);
                 });
    }
}

pub struct BlobQuery;

// required?
graphql_object!(BlobQuery: Storage |&self| {
    field blob(&executor, id: String) -> FieldResult<Blob> {
        let ctx = executor.context();
        match ctx.vmap.borrow().get(&id) {
            Some(ref nb) => {
                let _nb = (*nb).clone();
                Ok(_nb.into_blob(&id))
            },
            None => Err(FieldError::new("could not find `Blob` id in hashmap", Value::Null))
        }
    }
});

pub struct BlobMutation;

graphql_object!(BlobMutation: Storage |&self| {});

pub type BlobSchema = juniper::RootNode<'static, BlobQuery, BlobMutation>;

#[allow(dead_code)]
fn show(x: impl Debug) { println!("{:#?}", x) }
