#[macro_use] extern crate juniper;

use juniper::{FieldResult, FieldError, Value};
use std::collections::hash_map::{HashMap, DefaultHasher};
use std::hash::{Hash, Hasher};
use std::cell::RefCell;

pub mod blob;

#[derive(GraphQLEnum, Debug, Clone, Hash)]
pub enum Episode {
    NewHope,
    Empire,
    Jedi,
}
//TODO: impl `FromInputValue` for `Human`

#[derive(Hash, Clone, Debug)]
//#[graphql(description="a humanoid creature")]
pub struct Human {
    id: String,
    name: String,
    appears_in: Vec<Episode>,
    home_planet: String,
    best_friend: Option<String>,
}

type Id = String;
// TODO: 
// 1. make error `Human` containing scope specification.
// 2, profit from helpful error messages!
graphql_object!(Human: Context |&self| {
    field id() -> &str { &self.id }
    field name() -> &str {&self.name }
    field appears_in() -> &[Episode] { &self.appears_in }
    field home_planet() -> &str { &self.home_planet }
    field best_friend(&executor) -> Option<Human> { 
        let nh_outer = Human {
                id: "no id".to_owned(),
                name: "outer err".to_owned(),
                appears_in: vec![],
                home_planet: "".to_owned(),
                best_friend: None,
        };

        let ctx = executor.context();

        // the below `bff` works, but requires what seems an needless lookup.
        // If `self.best_friend` wasn't None, this could be improved
        // significantly.
        
        //let bff = ctx.db.borrow().get(&self.id).unwrap().best_friend.clone();
        let keanes_id = "15097517514617858941".to_owned();
        //println!("best_friend: ctx:  db:{:#?}\nnames:{:#?}", ctx.db, ctx.names);
        //println!("self {:#?}", self);
        // replace `self.best_friend` with `bff`, which is "fresher".
        match self.best_friend {
            Some(ref id) => {
                match ctx.db.borrow().get(id) {
                    Some(ref nh) => Some(new_human_to_human((*nh).clone(), id.to_owned())),
                    None => None,
                }
            },
            None => Some(nh_outer),

        }
    }
});

#[derive(GraphQLInputObject, Debug, Hash, Clone)]
#[graphql(description="a humanoid creature")]
pub struct NewHuman {
    pub name: String,
    pub appears_in: Vec<Episode>,
    pub home_planet: String,
    pub best_friend: Option<Id>,
}

impl NewHuman {
    pub fn add_best_friend(mut self, friend: String) -> Self {
        self.best_friend = Some(friend);
        self


    }
}

fn new_human_to_human(new_human: NewHuman, id: String) -> Human {
    Human { id: id,
            name: new_human.name,
            appears_in: new_human.appears_in,
            home_planet: new_human.home_planet,
            best_friend: new_human.best_friend,

    }
}

pub struct Context {
    pub db: RefCell<HashMap<String, NewHuman>>,
    pub names: RefCell<HashMap<String, (NewHuman, String)>>,
}

impl Context {
    pub fn new() -> Self {
        let db = RefCell::new(HashMap::new());
        let names = RefCell::new(HashMap::new());
        Context { db, names }
    }

    // Wrapper around `HashMap::insert`
    pub fn insert(&mut self, v: NewHuman) -> Option<NewHuman> {
        let id = gen_human_id(&v).to_string();
        let name = v.name.clone();
        self.names.borrow_mut().insert(name, (v.clone(), id.clone()));
        self.db.borrow_mut().insert(id, v)
        

    }
}

impl juniper::Context for Context {}

pub struct Query;


graphql_object!(Query: Context |&self| {
    field apiVersion() -> &str {
        "1.0"
    }
    field humanById(&executor, id: String) -> FieldResult<Human> {
        let ctx = executor.context();
        match ctx.db.borrow().get(&id) {
            Some(ref human) => Ok(new_human_to_human((*human).clone(), id)), // wtf?
            None => Err(FieldError::new( "could not find `Human` id in hashmap", Value::Null))
        }

    }
    //field name() -> &str {
    //    &self.name
    //}
    //field home_planet() -> &str {
    //    &self.home_planet
    //}
    //field appears_in() -> &[Episode] {
    //    self.appears_in.as_slice()
    //}


    field human(&executor, name: String) -> FieldResult<Human> {
        let ctx = executor.context();
        match ctx.names.borrow().get(&name) {
            Some(&(ref human, ref id)) => Ok(new_human_to_human((*human).clone(), (*id).clone())),
            None => Err(FieldError::new("could not find `Human` name in hashmap", Value::Null))
        }
    }
});

/// Hash the given `Hashable` value with the provided `Hasher`.
pub fn hash_with(x: &impl Hash, hasher: &mut impl Hasher) -> u64 {
    x.hash(hasher);
    hasher.finish()
}

/// Hash a `Hashable` with the `DefaultHasher`.
pub fn hash_default(x: &impl Hash) -> u64 {
    let mut h = DefaultHasher::new();
    hash_with(x, &mut h)
}


pub fn gen_human_id(new_human: &NewHuman) -> u64 {
    let mut s = DefaultHasher::new();
    new_human.hash(&mut s);
    s.finish()
}

pub struct Mutation;

graphql_object!(Mutation: Context |&self| {
    field createHuman(&executor, new_human: NewHuman) -> FieldResult<Human> {
        let db = &executor.context().db;
        let id = gen_human_id(&new_human).to_string();
        let human = new_human_to_human(new_human.clone(), id.clone());
        match db.borrow_mut().insert(id, new_human) {
            Some(replaced) => Ok(human),
            None => Ok(human),
        }
    }
});

pub type Schema = juniper::RootNode<'static, Query, Mutation>;



