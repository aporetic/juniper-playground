extern crate note_juniper;
extern crate juniper;
extern crate serde_json;

use juniper::{InputValue, Variables};
use note_juniper::*;
use serde_json::to_string_pretty;

use std::fmt::Debug;

fn main() { run_blob(); }

#[allow(dead_code)]
fn main_old() {
    let mut ctx = Context::new();

    let nh0 = NewHuman { name: "chase".to_owned(),
                         appears_in: vec![Episode::NewHope],
                         home_planet: "Earth".to_owned(),
                         best_friend: None};

    let id0 = gen_human_id(&nh0).to_string();
    //println!("main: id0: {}", id0);

    let nh1 = NewHuman { name: "keane".to_owned(),
                         appears_in: vec![Episode::Empire],
                         home_planet: "Mars".to_owned(),
                         best_friend: Some(id0.clone()) };
    //let (_, id1) = ctx.names.borrow().get("keane").unwrap().clone();
    let id1 = "15097517514617858941".to_owned();
    
//    println!("main: nh1: {:?}", nh1);
    ctx.insert(nh0);
    ctx.insert(nh1.clone());
 //   println!("main: ctx post insert: db:{:#?}\nnames:{:#?}", ctx.db, ctx.names);
    match ctx.db.borrow().get(&id1) {
        Some(_) => {} //println!("some: {:#?}", human),
        None => {}
    }
    //println!("main: ctx get(): {:#?}", ctx.db.borrow().get(&id0));

    //let name = "name".to_owned();
    //let id = "4937464294728694047".to_owned();
    //let appears_in = vec![Episode::Empire];
    //let home_planet = "Mars".to_owned();
    let empty_mut: Mutation = Mutation {};
    let query = "\
query { 
    human(name: \"keane\") { 
        id
        name
        bestFriend {
            id
            name
            bestFriend {
                id
                name
                homePlanet
                appearsIn
                }

        }
    } 
}";
    let mut vars = Variables::new();
    vars.insert("id".to_owned(), InputValue::String(id0.clone()));
    let (res, _errs) = juniper::execute(
        //"query { humanById (id: \"4937464294728694047\") { id\nname\nhomePlanet\nappearsIn } }",
        //"query { human(name: \"keane\") { id\nname\nhomePlanet\nappearsIn\nbestFriend {id\nname}} }",
        query,
        None,
        &Schema::new(Query, empty_mut),
        //&vars,
        &Variables::new(),
        &ctx,
        ).unwrap();
    //println!("{:?}", nh1);
    println!("{}", to_string_pretty(&res).unwrap());
    //println!("{:?}", id0);
}

use note_juniper::blob::*;

fn run_blob() {
    let mut ctx = Storage::new();

    let b0 = NewBlob {
        content: "b0".to_owned(),
        points_to: vec![],
        is_pointed_to_by: vec![],
    };
    let _id0 = ctx.insert(b0.clone());

    let b1 = NewBlob {
        content: "b1".to_owned(),
        points_to: vec![],
        is_pointed_to_by: vec![],
    };
    // anamorphism, analysis, lysis
    let _id1 = ctx.insert(b1.clone());


    let _b2 = NewBlob {
        content: "b2".to_owned(),
        points_to: vec![],
        is_pointed_to_by: vec![],
    };

    // graph 0
    let mut ctx0 = Storage::new();
    let g0_b0 = b0.clone();
    let g0_b1 = b1.clone();
    let g0_b2 = _b2.clone();

    let (g0_id0,_) = ctx0.insert(g0_b0);
    let (g0_id1,_) = ctx0.insert(g0_b1);
    let (g0_id2,_) = ctx0.insert(g0_b2);

    // link b1 -> b0
    ctx0.link(&g0_id1, &g0_id0);
    ctx0.link(&g0_id2, &g0_id1);
    //show(ctx0);
    //show(g0_b0);
    let _empty_mut = BlobMutation {};
    let mut _vars = Variables::new();
    let query = format!("\
query {{ 
    blob(id: \"{}\") {{ 
        id
        content
        pointsTo {{ id
            content
            pointsToIds 
        }}
    }} 
}}", g0_id1);

    let (res, _errs) = juniper::execute(
        //"query { humanById (id: \"4937464294728694047\") { id\nname\nhomePlanet\nappearsIn } }",
        //"query { human(name: \"keane\") { id\nname\nhomePlanet\nappearsIn\nbestFriend {id\nname}} }",
        &query,
        None,
        &BlobSchema::new(BlobQuery, _empty_mut),
        &_vars,
        &ctx0
        ).unwrap();
    //println!("{:?}", nh1);
    println!("{}", to_string_pretty(&res).unwrap());

}
#[allow(dead_code)]
fn get_id(x: Option<(BlobId, NewBlob)>) -> BlobId { 
    println!("{:#?}", x);
    x.map(|(x,_)|x).unwrap().clone() 
}

#[allow(dead_code)]
fn show(x: impl Debug) { println!("{:#?}", x) }
